//define function
function fillFields (field_type)
{
    if (field_type == fields)
    {
        var field_value = "markus";        
    }
     
    if (field_type == e_mail_fields)
    {
        var field_value = "noreply@sbb.ch";        
    }
    
    if (field_type == number_fields)
    {
        var field_value = "10";        
    }
     
    if (field_type == textarea_fields)
    {
        var field_value = "form-test";         
    }
     
    if (field_type == fone_fields)
    {
        var field_value = "+77 7777 7777 7777 777";        
    }
     
    if (field_type == date_fields)
    {
        var field_value = "Di, 14.07.1789";        
    }
     
    i = field_type.length;
    var x = 0;
    for (x; x < i; x++) {
    field_type[x].value = field_value;
    }
}
 
//declare variables - date is in input[type='text']
var fields = document.querySelectorAll("input[type='text'], input[type='email'], input[type='tel'], textarea");
 
var fields = document.querySelectorAll("input[type=text]");
var e_mail_fields = document.querySelectorAll("input[type='email']");
var number_fields = document.querySelectorAll("input[type='number']");
var textarea_fields = document.querySelectorAll("textarea");
var fone_fields = document.querySelectorAll("input[type='tel']");
var date_fields = document.getElementsByClassName("mod_datepicker_input");
 
var radiobutton_checkbox_fields = document.querySelectorAll("input[type='radio'], input[type='checkbox']");
 
//call function
fillFields(fields);
fillFields(e_mail_fields);
fillFields(number_fields);
fillFields(textarea_fields);
fillFields(fone_fields);
fillFields(date_fields);
 
//check radioboxes and checkboxes
for (n = 0; n < radiobutton_checkbox_fields.length; n++)
{
    radiobutton_checkbox_fields[n].checked = true; 
}